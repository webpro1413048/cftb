import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLaodStore = defineStore('laoding', () => {
  const islaodding = ref(false)
  const doLaod = () => {
    islaodding.value = true
  }
  const finsish = () => {
    islaodding.value = false
  }
  return { islaodding, doLaod, finsish }
})
