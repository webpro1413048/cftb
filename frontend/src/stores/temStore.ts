import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import http from '@/services/http'

export const useTemStore = defineStore('tem', () => {
  const valid = ref(false)
  const result = ref(0)
  const celsius = ref(0)

  type ReturnDeta = {
    celsius: number
    fahrenheit: number
  }
  async function callConvert() {
    // result.value = convert(celsius.value)
    try {
      console.log(`/temperature/convert/${celsius.value}`)
      const res = await http.post(`/temperature/convert`, {
        celsius: celsius.value
      })

      const convertResult = res.data as ReturnDeta
      console.log(convertResult)
      result.value = convertResult.fahrenheit
    } catch (e) {
      console.log(e)
    }
  }

  return { valid, result, celsius, callConvert }
})
